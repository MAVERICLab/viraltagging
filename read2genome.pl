$file = $ARGV[0];

################READS

open ($FH, '<', $file) or die "Cannot open $file\n";
while ($line = <$FH>) {
	chomp ($line);
	if ($line =~ />(.+)/) {
		$name = $1;
		$read{$name}='';
		$ord++;
	}
	else{
		$read{$name}=$read{$name}.$line;
	}
}
close($FH);

#############GENOME##################################

$file1 = $ARGV[1];

open ($FH, '<', $file1) or die "Cannot open $file1\n";
while ($line = <$FH>) {
chomp($line);
	if ($line =~ />(.+)/ ) {
	$name = $1;
	$genome{$name}= '';
	} else {
		$genome{$name}=$genome{$name}.$line;
	}	
}
close($FH);

$lg = length($genome{$name});
@gen = split (//, $genome{$name});
for $k (0..$lg) {
	$dif{$k} =0;
	$gcov{$k}=0;
	$snp{$k}='';
	$nuc_dif{$k}='';
}

#################BLAST##############################

$file2 = $ARGV[2];
$tag = substr($file2, 0, -4);

$id = "XXX";

$reads = 0;

open ($FH, '<', $file2) or die "Cannot open $file2\n";
while ($line = <$FH>) {
chomp ($line);
      @blast = split (/\t/, $line);
  if ($blast[0] ne $id ) {    	
	$len = length($read{$blast[0]});
        $cov = ($blast[7] - $blast[6] + 1 )/$len;
	if ($cov > 0.90 && $blast[2] > 95) {        
	$reads++;
			if ($blast[9] > $blast[8]) {
				$ln = $blast[9]-$blast[8] +1;
				$sub = substr($genome{$blast[1]}, $blast[8]-1, $ln);
				$lnrd = $blast[7]-$blast[6] + 1;
				$sbrd = substr($read{$blast[0]}, $blast[6]-1, $lnrd);

			} else {
                    												#   if ($blast[8] > $blast[9]) {
					$ln = $blast[8]-$blast[9] + 1;
					$sub = substr($genome{$blast[1]}, $blast[9] - 1, $ln);
					$lnrd = $blast[7]-$blast[6] + 1;
					$sbrd = substr($read{$blast[0]}, $blast[6]-1, $lnrd);
					$sbrev = reverse($sbrd);
					$sbrev =~ tr/ATGC/TACG/;
					$sbrd = $sbrev;
		        }
				
				$output = "TEMP.txt";
				open ($OUT, '>', $output);
				print $OUT ">REFE\n$sub\n>READ\n$sbrd\n";
				close($OUT);
			
				$cmd = "muscle -in TEMP.txt -out ALN";
				`$cmd`;				
				
				open($AL, '<', "ALN");
				while($line = <$AL>) {
				chomp($line);
					if ($line =~ />(.+)/ ) {
		
						$name = $1;
						$seqal{$name} = '';
	 				} else {
						$seqal{$name} = $seqal{$name}.$line;
					}
				}
				close($AL);

				@array1 = split (//, $seqal{'REFE'});	
				@array2 = split (//, $seqal{'READ'});
			
	if ($seqal{'REFE'} !~ /\-/ ) {  ## &&  $seqal{'READ'} !~ /\-/ ) {  ###TO AVOID HYPHENS
	       if ( $blast[8] > $blast[9] ) {
			$ct = $blast[9]+1;	
	       } else {
			$ct = $blast[8]+1;
	       }
			for $i (0..$#array1) {
				$nuc_dif{$ct}=$nuc_dif{$ct}.$array2[$i];		
				$gcov{$ct}++;
				$ct++;			
			}
	} ###HYPHENS
        }
  }

	$id =$blast[0];
}
close($FH);


$output = "$tag".".COV";
open($OUT, '>', $output);
print $OUT "$tag"."_"."$reads\n";
for $j (0..$lg) {
		print $OUT "$j\t$gen[$j]\t$gcov{$j+2}\t$nuc_dif{$j+2}\n";

}











