#!usr/bin/perl 

$file1 =$ARGV[0];

$id = 1;

##READ THE READ2COG FILE###    TAB limited file.  READ\tCLUSTER\n

open ($FH, '<', $file1);
while ($line = <$FH>) {
chomp($line);
    @blast = split (/\t/, $line);
    $cog{$id} = $blast[1];
    $id++;                
}
close($FH);

###########################

$sample = 5000;          ## Sample size, number of sampled reads, increments at each step.


while ($sample < 225000) { #### TOTAL number of reads
print "$sample\t";
  for $m (0..100) {             ###where $m is the number of randomizations, to generate a smooth curve
      %dif = ();
      $ct = 0;
      %used= ();
      
      while ($ct < $sample) {
           $seed = int(rand(225000));  ###TOTAL number of reads, same number as line 23
          if (!exists($used{$seed})) {        #### The reads are resampled only ONCE. A read cannot be accounted twice in the same round.
           $used{$seed}++;      
           $dif{$cog{$seed}}++;
           $ct++;
          }

      }
      
      $total = (keys %dif); 
      print "$total\t";
      $m++;
  }
        $sample = $sample + 5000;      ### Incremental interval.
        print "\n";
}

