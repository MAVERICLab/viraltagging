"""
Owner: Julio Ignacio Cesar

Author: Zachary Hare

Name: Recruit2cloud.py

Usage:
    -h --help                  Print Usage
    -m --man                   Print POSIX Manual

    -g --genomes               Number of metagenomes to generate
    -b --blast                 File containing the blastn results of the
                                reference genome
    -o --output                Filepath to generate output files to
    -t --tag                   Tag to put on the front of the names of the
                                output files.
    -c --cov                   Defines whether to also save the .COV file 
                                generated. By default this gets cleaned at the 
                                end of the program.

Program Description:

    Recruit2cloud.py takes the XML version of a BLASTN alignment of a series of reads vs a reference genome. It creates a "cloud" of pseudo-random genomes similar to the reference genome. If a BLASTN alignment is not provided the program exits. If the number of genomes isn't provided, it defaults to 10. If the output directory isn't specified, it defaults to the current working directory. If the tag isn't specified, it defaults to "Recruit2cloudOutput_". If a .COV output file isn't specified then the intermediate base alignment isn't printed.

The options are as follows:
    -h|--help|--man Optional. Print documentation to STDOUT

    -b|--blast= Required. Specify the XML-formatted BLASTN alignment file

    -g|--genomes= Optional. Specify the number of metagenomes to generate

    -o|--output= Optional. Specify the directory to print the metagenomes to

    -t|--tag Optional. Specify what the metagenomes will be named

    -c|--cov Optional. Specify the output file for the intermediate alignment
    data structure

Exit Status: Recruit2cloud dies on error, returns 0 on success.

Examples:
    The command:
        python3 Recruit2cloud.pl -b blastn.xml -g 1 -o ~/ -t R2C_ -c ~/testcov.COV
    will output 1 randomized genome to ~/ with a name of R2C_0 and it will generate the intermediate .COV file to ~/testcov.COV.

Files:
    in: 1 user-defined .xml file
    out: 1 or more .META files and 1 file with a text representation of the alignment data structure.

Dependencies:
    Python 3
    BioPython

Change Log:
    None

Copyright 2012 UA TMPL

Permission is granted to copy, distribute and/or modify this 
document under the terms of the GNU Free Documentation 
License, Version 1.2 or any later version published by the 
Free Software Foundation; with no Invariant Sections, with 
no Front-Cover Texts, and with no Back-Cover Texts.
"""

# from multiprocessing import Process, Queue, current-Process, freeze_support
import threading
from threading import Thread
import time
import random
import sys, getopt
import Bio
from Bio.Blast import NCBIXML
from array import *

class Repeat:
    def __init__(self, char):
        self._char = char
        self._count = 1

    def __eq__(self, other):
        return self._char == other._char

    def __hash__(self):
        return (self._char * 1024 % self._count)

    def __repr__(self):
        return '%s%d' % (self._char, self._count)

# Parameter containers
genomes = 10
blast = None
output = "./"
tag = "Recruit2cloudOutput_"
cov = None

# Prints a metagenome, given the COV construct basesPerPosition and which
# number to tag it with
class printGenomes(Thread):
    def __init__(self, basesPerPosition, i):
        self.basesPerPosition = basesPerPosition
        self.i = i
        Thread.__init__(self)

    def run(self):
        outFile = open((output + tag + self.i.__repr__() + ".META"), "w")

        lineCounter = 0

        # For each position, pick a random base and write it out to the file
        for position in self.basesPerPosition:
            # If nothing aligned to this position, discard the position
            if position[0] == -1:
                continue
            
            # To make it human-readable, break on every 60th base
            if lineCounter == 60:
                outFile.write("\n")
                lineCounter = 0

            # Else choose a random letter and write it to the file
            select = random.random()
            if select < position[0]:
                outFile.write("A")
            elif select < (position[0] + position[1]):
                outFile.write("G")
            elif select < (position[0] + position[1] + position[2]):
                outFile.write("C")
            else:
                outFile.write("T")

            lineCounter += 1

        outFile.close()

class Recruit2cloud():
    def main():
        global genomes
        global blast
        global output
        global tag
        global cov

        # Begin parameter parsing
        usage = """
            python Recruit2cloud.py [-h|--help] [-m|--man] [-g|--genomes] <integer>
            [-b|--blast] <[file]> [-o|--output] <[file]> [-t|--tag] <string> [-c|--cov]
            <file>
        """

        # Try to get the command line options given
        try:
            opts, args = getopt.getopt(sys.argv[1:], "hmg:b:o:t:c:", 
                    ["help", "man", "genomes=", "blast=", "output=", "tag=", "cov="])

        # If we couldn't figure out how to parse the options, throw an error and exit
        except getopt.GetoptError as err:
            print(str(err))
            print(usage)
            sys.exit(2)

        # Loop through the arguments passed and try to assign their values to the
        # right variables
        for k,v in opts:
            if k == '-h' or k == "--help":
                print(usage)
                sys.exit(0)
            elif k == '-m' or k == "--man":
                # print(self.to_string(self))
                print(usage)
                sys.exit(0)
            elif k == '-g' or k == "--genomes":
                genomes = int(v)
            elif k == '-b' or k == "--blast":
                blast = v
            elif k == '-o' or k == "--output":
                output = v
            elif k == '-t' or k == "--tag":
                tag = v
            elif k == '-c' or k == "--cov":
                cov = v
            # Otherwise they passed a bad argument, complain and quit
            else:
                print("Invalid parameter: ", k)
                print(usage)
                sys.exit(2)
        # End parameter parsing

        blastReader = open(blast, "r")

        # Parse the BLAST file
        blast_records = NCBIXML.parse(blastReader)

        # Initialize the list of lists that will contain the per-position base
        # structure
        record = blast_records.__next__()
        basesPerPosition = [list() for i in range(0, record.database_length)]

        # Because the iterator consumed the first input, and because the NCBIXML
        # parser doesn't implement a rewind function, need to reset the iterator
        # before going over every object
        blastReader.close()
        blastReader = open(blast, "r")
        blast_records = NCBIXML.parse(blastReader)

        # Process the data that BLAST spat out
        for record in blast_records:
            # If the current read didn't have an alignment, check the next read
            if len(record.alignments) == 0: continue

            # Otherwise get the info of the alignment
            align = record.alignments[0].hsps[0]

            # If the current read didn't have at least 95% identity, discard it
            if (align.identities / len(align.query)) < 0.95: continue

            # Get the indeces of where the read was matched in the reference
            # Note that we reduce each index by 1, since BLASTN indexes starting at 1
            # but the list starts at 0 
            # If the alignment was backwards, we need to index backwards
            if align.sbjct_start > align.sbjct_end:
                indeces = reversed(range(align.sbjct_end-1, align.sbjct_start-1))
            else:
                indeces = range(align.sbjct_start-1, align.sbjct_end-1)

            # Start pushing the characters at each index into the base structure
            count = 0
            for i in indeces:
                # Get the list at this position in the genome
                position = basesPerPosition[i]

                # If the list is empty or the most recent character is different, add a
                # new node to the list
                if len(position) == 0 or align.query[count] != position[len(position)-1]._char:
                    position.append(Repeat(align.query[count]))

                # Else the current base is the same as the most recent one in the list;
                # increment the count
                else:
                    position[len(position)-1]._count += 1

                count += 1

        # Done with the BLAST file
        blastReader.close()

        # Once this has to page out the above check will change to (if not cov:
        # delete the page file) If the user wants the .COV generated, do it
        if cov:
            covout = open(cov, "w")
            for position in basesPerPosition:
                for base in position:
                    covout.write(base.__repr__())
                covout.write("\n")
            covout.close()

        # Build the probabilities array
        for i in range(0, len(basesPerPosition)):
            # Initialize the array of how many of each position we saw
            counts = array('f')
            for j in range(0,4): counts.append(0)

            # For each base we saw, add how many times we saw it to the probabilities
            # array
            for repeat in basesPerPosition[i]:
                if repeat._char == "A":
                    counts[0] += repeat._count
                elif repeat._char == "G":
                    counts[1] += repeat._count
                elif repeat._char == "C":
                    counts[2] += repeat._count
                elif repeat._char == "T":
                    counts[3] += repeat._count

            # Replace the COV data structure with the counts
            basesPerPosition[i] = counts;
            
            # Now calculate the actual probability
            total = counts[0] + counts[1] + counts[2] + counts[3]

            # If nothing aligned to this position in the reference, mark it and move on
            # to the next position
            if total == 0:
                counts[0] = -1
                continue

            # Otherwise calculate the probabilities
            counts[0] = counts[0] / total
            counts[1] = counts[1] / total
            counts[2] = counts[2] / total
            counts[3] = counts[3] / total

        # Scan the probabilities array <genomes> times to create a new metagenome
        liveThreads = list()
        while (genomes >= 1):
            lTCopy = list()
            # If we already have 4 threads active, check to see if any have finished
            if len(liveThreads) >= 4:
                for thread in liveThreads:
                    # If the thread is still running, copy its pointer
                    if thread.isAlive():
                        lTCopy.append(thread)

            # If the queue is full, then all 4 threads are still running, so wait a bit
            if len(lTCopy) == 4:
                time.sleep(0.01)
                continue

            # Else we can spawn more threads
            else:
                while len(lTCopy) < 4 and genomes >= 1:
                    tmp = printGenomes(basesPerPosition, genomes)
                    tmp.start()
                    lTCopy.append(tmp)
                    genomes -= 1

            # Finally, copy the new list to liveThreads for the next check
            liveThreads = lTCopy

if __name__ == "__main__":
    Recruit2cloud.main()
