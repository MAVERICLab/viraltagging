function Q = AccRdm(index,assigned); %% M = Matrix of presence absence; NAMES = vetor with taxa names.
Q =  zeros(10000,1);
for i = 1:10000                 %%
    r = index(randperm(length(index)));       %% Size of the matrix
    
    q = Acc(r,assigned);
    
    Q(i)=q;
    
end
return;