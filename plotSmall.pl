$file1 = $ARGV[0];

################ BLASTP

open ($FH, '<', $file1) or die "Cannot open $file1\n";
while ($line = <$FH>) {
	chomp ($line);
		@data = split (/\t/, $line);
		@contig = split (/\_/, $data[0]);
	$max{$contig[0]} = 0;
	$min{$contig[0]} = 100000000;
}
close($FH);


open ($FH, '<', $file1) or die "Cannot open $file1\n";
while ($line = <$FH>) {
	chomp ($line);
	  
		@data = split (/\t/, $line);
		@contig = split (/\_/, $data[0]);
		@names  = split (/\#/, $data[1]);
		
if ($max{$contig[0]} < $names[2] ) {
	$max{$contig[0]} = $names[2];
}
if ($min{$contig[0]} > $names[1] ) {
	$min{$contig[0]} = $names[1];
}

}
close($FH);

foreach $key (keys %min ) {
	$avg = ($max{$key} + $min{$key})/2;
	$central{$key} = $avg; 

}


$file = $ARGV[1];

################READS

open ($FH, '<', $file) or die "Cannot open $file\n";
while ($line = <$FH>) {
	chomp ($line);
	
	if ($line =~ />(.+)/) {
		@names = split (/\s+/, $1);

		$len{$names[0]} = ($names[-1])/2
	}
}
close($FH);

$ct = 200;

foreach $key (sort {$central{$a} <=> $central{$b} } keys %central) {
	$corr{$key} = $central{$key}-$len{$key} + 5000;
	$ct = $ct + 100;
	$y{$key} =$ct;
	
}

$file2 = $ARGV[2];

open ($FH, '<', $file2) or die "Cannot open $file\n";
while ($line = <$FH>) {
	chomp ($line);
	
	if ($line =~ />(.+)/) {
		@data = split (/\s+/, $1);
		@contig = split (/\_/, $data[0]);
		$x1= $data[2] + $corr{$contig[0]};
		$x2= $data[4] + $corr{$contig[0]};
		$y1 = $y{$contig[0]};
		$y2 = $y{$contig[0]} + 25;

if ($y1 ne '') {
	#	print "$contig[0]\t$data[2]\t$data[4]\t$corr{$contig[0]}\n";
		print "set arrow from $x1,$y1 to $x2,$y1 as 1\n";
		print "set arrow from $x1,$y2 to $x2,$y2 as 1\n";
		print "set arrow from $x2,$y1 to $x2,$y2 as 1\n";
		print "set arrow from $x1,$y1 to $x1,$y2 as 1\n";
}
	}
}
close($FH);

