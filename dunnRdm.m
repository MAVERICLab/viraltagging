function D = dunnRdm(M,ind); %% M = Matrix of presence absence; NAMES = vetor with taxa names.
D =  zeros(1000,1);
for i = 1:1000                 %%
    r = ind(randperm(length(ind)));       %% Size of the matrix
    
    di = dunns(15,M,r)
    
    D(i)=di;
    
end
return;
