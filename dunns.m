function DI=dunns(total,Matrix,ind)   
% total = Number of clusters 
% Matrix = Distance
% ind   = Indexes for each data 
i=total;
denominator=[];

for j=1:i
    indi=find(ind==j);
    indj=find(ind~=j);
    x=indi;
    y=indj;
    temp=Matrix(x,y);
    denominator=[denominator;temp(:)];
end

num=min(min(denominator)); 
neg_obs=zeros(size(Matrix,1),size(Matrix,2));

for k=1:i
    indxs=find(ind==k);
    neg_obs(indxs,indxs)=1;
end

dem=neg_obs.*Matrix;
dem=max(max(dem));

DI=num/dem;
end